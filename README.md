Terraform AWS Application Load Balancer Module
==============================================

Terraform module to create following AWS resources:
- Application Load Balancer and associated Security Group
- Application Load Balancer Listeners
- Application Load Balancer Target Group

# Contents
- [Required Input Variables](#variables)
- [Usage](#usage)
- [Outputs](#outputs)
- [Licence](#licence)
- [Author Information](#author)

## <a name="variables"></a> Required Input Variables
At least following input variables must be provided. See [full list](variables.tf) of supported variables

| Name         | Description                             |
| ------------ | --------------------------------------- |
| name         | Common name - unique identifier         |
| vpc_id       | ID of a VPC resource will be created in |
| subnet_ids   | Subnet IDs service will be attached to  |

## <a name="usage"></a> Usage
```hcl-terraform
module "network" {
  source     = "git::https://gitlab.com/HippoLab/terraform-modules/aws-network.git"
  name       = "${var.project_name} ${local.environment}"
  extra_tags = local.common_tags
}

module "frontend_alb" {
  source     = "git::https://gitlab.com/HippoLab/terraform-modules/aws-alb.git"
  name       = "${var.project_name} ${local.environment} Frontend"
  vpc_id     = module.network.vpc_id
  internal   = local.environment != "production" ? true : false
  subnet_ids = module.network.subnet_ids["public"]
  listeners = [
    {
      name     = "HTTP"
      protocol = "HTTP",
      port     = 80,
      action   = "redirect"
    },
    {
      name     = "HTTPS"
      protocol = "HTTPS",
      port     = 443,
      action   = "forward"
    }
  ]
  certificate_arn   = aws_acm_certificate_validation.environment_wildcard.certificate_arn
  health_check_port = 8080
  target_type       = "ip"
  access_logging = [
    {
      access_logs_bucket        = aws_s3_bucket.logging_bucket.id
      access_logs_bucket_prefix = "frontend_alb"
      logging_enabled           = true
    }
  ]
  extra_tags = local.common_tags
  depends_on = [
    aws_s3_bucket_policy.logging_bucket // Creation of an ALB fails if permissions are not granted before
  ]
}

resource "aws_security_group_rule" "frontend_alb_http_ingress" {
  security_group_id = module.frontend_alb.sg_id
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Internet"
}

resource "aws_security_group_rule" "frontend_alb_https_ingress" {
  security_group_id = module.frontend_alb.sg_id
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 443
  to_port           = 443
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Internet"
}
```

## <a name="outputs"></a> Outputs
Full list of module outputs and their descriptions can be found in [outputs.tf](outputs.tf)

## <a name="licence"></a> Licence
The module is being distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed
to its terms and conditions

## <a name="author"></a> Author Information
Vladimir Tiukhtin <vladimir.tiukhtin@hippolab.ru><br/>London
