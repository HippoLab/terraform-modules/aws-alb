output "name" {
  description = "Name of the alb"
  value       = aws_lb.alb.name
}

output "alb_arn" {
  description = "The ID (ARN) of the created ALB"
  value       = aws_lb.alb.arn
}

output "alb_dns_name" {
  description = "The DNS name of the ALB presumably to be used with a friendlier CNAME"
  value       = aws_lb.alb.dns_name
}

output "listeners" {
  description = "ARN of the named listener"
  value = {
    for listener in var.listeners : listener["name"] =>
    {
      arn = aws_lb_listener.listener[listener["name"]]["arn"]
    }
  }
}

output "sg_id" {
  value = aws_security_group.alb_default_sg.id
}

output "alb_zone_id" {
  description = "The zone_id of the ALB"
  value       = aws_lb.alb.zone_id
}

output "target_group_arn" {
  description = "ARN of the target group"
  value       = aws_lb_target_group.target_group.arn
}

output "target_group_name" {
  description = "ARN of the target group"
  value       = aws_lb_target_group.target_group.name
}
