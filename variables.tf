variable "name" {
  description = "Common name, a unique identifier"
  type        = string
}

variable "internal" {
  description = "Whether ot not ALB must be Internet facing"
  type        = bool
  default     = false
}

variable "listeners" {
  description = ""
  type = list(object(
    {
      name     = string
      protocol = string
      port     = number
      action   = string
    }
  ))
  default = [
    {
      name     = "HTTP"
      protocol = "HTTP",
      port     = 80,
      action   = "forward"
    }
  ]
}

variable "vpc_id" {
  description = "ID of an existing VPC"
  type        = string
}

variable "subnet_ids" {
  description = "Subnet IDs to attach ALB to"
  type        = list(string)
}

variable "ipv6_enabled" {
  description = "Whether or not IPv6 should be enabled"
  type        = bool
  default     = true
}

variable "access_logging" {
  description = "List of maps containing access_logging settings"
  type = list(object(
    {
      access_logs_bucket        = string
      access_logs_bucket_prefix = string
      logging_enabled           = bool
    }
  ))
  default = []
}

variable "instance_port" {
  description = "The port the service on the EC2 instances listen on"
  type        = number
  default     = 80
}

variable "certificate_arn" {
  description = "ARN of the SSL Certificate. Required when provisioning HTTPS listener"
  type        = string
  default     = ""
}

variable "ssl_policy" {
  description = "Pre-defined TLS negotiation configuration"
  type        = string
  default     = "ELBSecurityPolicy-TLS-1-2-2017-01"
}

variable "interval" {
  description = "Interval at which to the store the logs"
  type        = number
  default     = 60
}

variable "security_policy" {
  description = "The security policy if using HTTPS externally on the ALB"
  type        = string
  default     = "ELBSecurityPolicy-2016-08"
}

variable "idle_timeout" {
  description = "ALB idle timeout value, the number of seconds to wait before an idle connection is closed"
  type        = number
  default     = 60
}

variable "instance_protocol" {
  description = "The instance protocol. Options: HTTP, HTTPS, TCP, SSL (secure tcp)"
  type        = string
  default     = "HTTP"
}

variable "target_type" {
  description = "The type of target, e.g. instance or ip"
  type        = string
  default     = "instance"
}

variable "stickiness" {
  description = "Use per-session load balancing rather than per-content request"
  type        = bool
  default     = false
}

variable "deregistration_delay" {
  description = "The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused"
  type        = number
  default     = 300
}

variable "cookie_duration" {
  description = "ALB stickiness cookie_duration"
  type        = number
  default     = 600
}

variable "http2_enabled" {
  description = "Choose whether to enable HTTP/2 on the load balancer"
  type        = bool
  default     = true
}

variable "deletion_protection_enabled" {
  description = "Choose whether to enable deletion protection on the load balancer"
  type        = bool
  default     = false
}

variable "health_check_interval" {
  description = ""
  type        = number
  default     = 5
}

variable "health_check_path" {
  description = "The URL the ELB should use for health checks. e.g. /health"
  type        = string
  default     = "/"
}

variable "health_check_port" {
  description = "The port used by the health check."
  type        = number
  default     = 80
}

variable "health_check_timeout" {
  description = ""
  type        = number
  default     = 3
}

variable "health_check_matcher" {
  description = "The success codes used by the health check"
  type        = list(string)
  default     = [200, 202]
}

variable "health_check_unhealthy_threshold" {
  description = "Number of failed health checks before marking as unhealthy"
  type        = number
  default     = 3
}

variable "health_check_healthy_threshold" {
  description = "Number of successful health checks before marking as healthy"
  type        = number
  default     = 3
}

variable "additional_security_group_ids" {
  description = "Any additional Security Group IDs to attach to the service"
  type        = list(string)

  validation {
    condition     = length(var.additional_security_group_ids) <= 4
    error_message = "Only 5 security groups are allowed per network interface. One has been already taken by the module."
  }

  default = []
}

variable "extra_tags" {
  description = "Map of additional tags to add to module's resources"
  type        = map(string)
  default     = {}
}
