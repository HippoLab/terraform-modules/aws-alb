resource "aws_lb" "alb" {
  name                       = substr(lower(replace(var.name, "/\\s/", "-")), 0, 32)
  subnets                    = var.subnet_ids
  security_groups            = compact(concat([aws_security_group.alb_default_sg.id], var.additional_security_group_ids))
  internal                   = var.internal
  enable_http2               = var.http2_enabled
  idle_timeout               = var.idle_timeout
  ip_address_type            = var.ipv6_enabled && !var.internal ? "dualstack" : "ipv4"
  enable_deletion_protection = var.deletion_protection_enabled

  dynamic "access_logs" {
    for_each = var.access_logging
    content {
      bucket  = access_logs.value.access_logs_bucket
      prefix  = access_logs.value.access_logs_bucket_prefix
      enabled = access_logs.value.logging_enabled
    }
  }

  tags = merge(local.common_tags, var.extra_tags)
}

resource "aws_security_group" "alb_default_sg" {
  name        = "${lower(replace(var.name, "/\\s/", "-"))}-alb"
  description = "${var.name} ALB Default Security Group"
  vpc_id      = var.vpc_id
  tags = merge(
    {
      Name = "${var.name} ALB"
    },
    local.common_tags,
    var.extra_tags
  )
}

resource "aws_lb_target_group" "target_group" {
  name                 = substr(lower(replace(var.name, "/\\s/", "-")), 0, 32)
  port                 = var.instance_port
  protocol             = var.instance_protocol
  vpc_id               = var.vpc_id
  target_type          = var.target_type
  deregistration_delay = var.deregistration_delay

  stickiness {
    type            = "lb_cookie"
    cookie_duration = var.cookie_duration
    enabled         = var.stickiness
  }

  health_check {
    interval            = var.health_check_interval
    path                = var.health_check_path
    port                = var.health_check_port
    healthy_threshold   = var.health_check_healthy_threshold
    unhealthy_threshold = var.health_check_unhealthy_threshold
    timeout             = var.health_check_timeout
    protocol            = var.instance_protocol
    matcher             = join(",", var.health_check_matcher)
  }

  tags = merge(local.common_tags, var.extra_tags)
}

resource "aws_lb_listener" "listener" {
  for_each = {
    for listener in var.listeners : listener["name"] => listener
  }

  load_balancer_arn = aws_lb.alb.arn
  port              = each.value["port"]
  protocol          = each.value["protocol"]
  ssl_policy        = each.value["protocol"] == "HTTPS" ? var.ssl_policy : null
  certificate_arn   = each.value["protocol"] == "HTTPS" ? var.certificate_arn : null

  default_action {
    type             = each.value["action"]
    target_group_arn = each.value["action"] == "forward" ? aws_lb_target_group.target_group.id : null

    dynamic "redirect" {
      for_each = each.value["action"] == "redirect" ? [
        {
          status_code = "HTTP_301"
          protocol    = "HTTPS"
          port        = 443
        }
      ] : []
      content {
        status_code = redirect.value["status_code"]
        protocol    = redirect.value["protocol"]
        port        = redirect.value["port"]
      }
    }
  }
}
